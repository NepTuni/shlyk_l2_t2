package com.example.shlyk_l2_t2;

public class PersonInfo {

    private String name;
    private String age;
    private int image;

    public PersonInfo(String name, String age, int image){
        this.name = name;
        this.age = age;
        this.image = image;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getAge(){
        return this.age;
    }

    public void setAge(String company){
        this.age = company;
    }

    public int getImage(){
        return this.image;
    }

    public void setImage(int image){
        this.image = image;
    }

}
