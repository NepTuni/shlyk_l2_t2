package com.example.shlyk_l2_t2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<PersonInfo> persons = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setInitialData();
        RecyclerView recyclerView = findViewById(R.id.list);
        DataAdapter adapter = new DataAdapter(this, persons);
        recyclerView.setAdapter(adapter);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_hamburger);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }




    private void setInitialData(){
        persons.add(new PersonInfo("Crosh", "5", R.drawable.crosh));
        persons.add(new PersonInfo("Crosh", "5", R.drawable.crosh));
        persons.add(new PersonInfo("Crosh", "5", R.drawable.crosh));
        persons.add(new PersonInfo("Crosh", "5", R.drawable.crosh));
        persons.add(new PersonInfo("Crosh", "5", R.drawable.crosh));
        persons.add(new PersonInfo("Crosh", "5", R.drawable.crosh));
        persons.add(new PersonInfo("Crosh", "5", R.drawable.crosh));
        persons.add(new PersonInfo("Crosh", "5", R.drawable.crosh));
        persons.add(new PersonInfo("Crosh", "5", R.drawable.crosh));
        persons.add(new PersonInfo("Crosh", "5", R.drawable.crosh));
        persons.add(new PersonInfo("Crosh", "5", R.drawable.crosh));





    }

}
