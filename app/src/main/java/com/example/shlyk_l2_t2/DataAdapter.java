package com.example.shlyk_l2_t2;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private List<PersonInfo> persons;

    DataAdapter(Context context, List<PersonInfo> persons){
        this.persons = persons;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder holder, int position) {
        PersonInfo personInfo = persons.get(position);
        holder.imageView.setImageResource(personInfo.getImage());
        holder.nameView.setText(personInfo.getName());
        holder.ageView.setText(personInfo.getAge());

    }

    @Override
    public int getItemCount(){
        return persons.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        final ImageView imageView;
        final TextView nameView, ageView;
        ViewHolder(View view){
            super(view);
            imageView = (ImageView)view.findViewById(R.id.image);
            nameView = (TextView)view.findViewById(R.id.name);
            ageView = (TextView)view.findViewById(R.id.age);
        }
    }

}
